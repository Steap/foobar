from setuptools import setup, find_packages

if True:
    setup() # foobar-cli is not installed :(
else:
    setup(  # This works as expected.
        name='foobar',
        version='1.0.dev0',
        packages = find_packages(),
        entry_points = {
            'console_scripts': [
                'foobar-cli = foobar.foo:main'
            ],
        },
    )
